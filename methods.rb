
def auth()
    uri = URI.parse('https://aboyarinov.amocrm.ru/private/api/auth.php?type=json')

    header = {
      'Content-Type': 'text/json'
    }
    data = {
      USER_LOGIN: 'a.boyarinov@gmail.com',
      USER_HASH:  '168ab2d58a85678e4ec250383418237ceb257f57'
    }

    open_ssl_con(uri)
    request = Net::HTTP::Post.new(uri.request_uri, header)
    request.body = data.to_json

    response = $https.request(request)
    cookie = response.response['set-cookie'].split('; ')[0]
    JSON.parse(response.read_body)
    return cookie
end
def piplines_list
  uri = URI("https://aboyarinov.amocrm.ru/api/v4/leads/pipelines")

  open_ssl_con(uri)
  request = Net::HTTP::Get.new(uri)
  request["Cookie"] = auth()
  response = $https.request(request)
  ap JSON.parse(response.read_body)
  #puts response.read_body

end

def contacts_list
  uri = URI("https://aboyarinov.amocrm.ru/api/v2/contacts")

  open_ssl_con(uri)
  request = Net::HTTP::Get.new(uri)
  request["Cookie"] = auth()
  response = $https.request(request)
  ap JSON.parse(response.read_body) if response.read_body
  #puts response.read_body

end

def contact(contact_id)
  uri = URI("https://aboyarinov.amocrm.ru/api/v4/contacts/" + contact_id.to_s)
  open_ssl_con(uri)
  request = Net::HTTP::Get.new(uri)
  request["Cookie"] = auth()
  response = $https.request(request)
  $arr = JSON.parse(response.read_body) if response.read_body

end

def contact_add(email)
  uri = URI.parse("https://aboyarinov.amocrm.ru/api/v2/contacts")
  open_ssl_con(uri)
  header = {
    'Content-Type': 'text/json'
  }
  request = Net::HTTP::Post.new(uri.request_uri, header)
  request["Cookie"] = auth()

  value_hash = {}
  values_arr = []
  values_hash = {}
  custom_fields_arr = []

  value_hash['value'] = email.to_s if email
  value_hash['enum'] = '110525'
  values_arr[0] = value_hash
  values_hash["id"] = '74205'
  values_hash["values"] = values_arr
  custom_fields_arr[0] = values_hash

  #email.hash -> values.arr -> values.hash -> custom fields.arr

  user = {}
  user["created_by"] = '6047044'
  user["custom_fields"] = custom_fields_arr
  user_add = []
  user_add[0] = user
  data = {}
  data["add"] = user_add
  #data["custom_fields_values"][0]["field_id"] = '74205'
  #data["custom_fields_values"][0]["values"][0]["values"]["value"] = email.to_s if email
  txt = data.to_json
  txt = txt.to_s
  request.body = txt
  #ap JSON.parse(request.body) if request.body

  response = $https.request(request)
  #ap JSON.parse(response.read_body) if response.read_body
  user_info = {}
  user_info['main'] = JSON.parse(response.read_body)
  user_id = user_info['main']['_embedded']['items'][0]['id'].to_s
  return user_id
end

def leads_list
  uri = URI("https://aboyarinov.amocrm.ru/api/v2/leads")

  open_ssl_con(uri)
  request = Net::HTTP::Get.new(uri)
  request["Cookie"] = auth()
  response = $https.request(request)
  # start a REPL session
  arr = JSON.parse(response.read_body) if response.read_body
  size_hash = arr.size
  ap arr
  i = 0
  email = []
  while i <= size_hash do
    lead_id = arr["_embedded"]["items"][i]["id"]
    status_id = arr["_embedded"]["items"][i]["status_id"]
    cost_lead = arr["_embedded"]["items"][i]["sale"]
    user_id = arr["_embedded"]["items"][i]["main_contact"]["id"]
    usr_hash = contact(user_id)

        for item in $arr["custom_fields_values"] do
              if item["field_id"] == 74205
                    n = 1
                        item["values"].each_with_index do |mails, index|
                            email[n-1] = mails["value"]
                            n += 1
                        end
              end
        end
    #email = usr_hash["custom_fields_values"][0]["values"][0]["value"]

    50.times {print "_"}
    puts "\n\n"
    puts "Индекс      : " + (i+1).to_s
    puts "Сделка     №: " + lead_id.to_s
    puts "Стоимость   : " + cost_lead.to_s
    puts "Пользователь: " + user_id.to_s
    email.each_with_index do |item, index|
      puts "Email № #{index+1}   : " + item
    end
    50.times {print "_"}
    puts ""
    i+=1
  end

  #puts response.read_body

end

def lead_add(price, user_id, pipline_id, status_id)
  uri = URI.parse("https://aboyarinov.amocrm.ru/api/v4/leads")
  open_ssl_con(uri)
  header = {
    'Content-Type': 'text/json'
  }
  request = Net::HTTP::Post.new(uri.request_uri, header)
  request["Cookie"] = auth()
  puts "привязать пользователя с id: #{user_id}"

  main_contact_arr = []
  main_contact_arr[0] = user_id.to_i
  deal = {}
  deal['name'] = "обращение от #{user_id}"
  deal['status_id'] = status_id.to_i
  deal['pipline_id'] = pipline_id.to_i
  deal['responseble_user_id'] = 6047044
  deal['created_by'] = 6047044
  #deal['contacts_id'] = main_contact_arr
  deal['price'] = price.to_i
  #data_hash = {}
  data = []
  data[0] = deal
  #data_hash["add"] = data
  txt = data.to_json
  #txt =  txt.to_s
  request.body = txt
  puts "до отправки, сформированный запрос"

  response = $https.request(request)
  ap JSON.parse(request.body) if request.body
  puts request.body
  puts "ответ сервера"
  response_res = JSON.parse(response.read_body) if response.read_body
  ap response_res
  contact_id = user_id
  lead_id = response_res["_embedded"]["leads"][0]["id"].to_i
  puts "lead id: #{lead_id}"
  link_to_contats(lead_id,contact_id)
end
def link_to_contats(lead_id,contact_id)
  uri = URI.parse("https://aboyarinov.amocrm.ru/api/v4/leads/#{lead_id}/link")
  open_ssl_con(uri)
  header = {
    'Content-Type': 'text/json'
  }
  request = Net::HTTP::Post.new(uri.request_uri, header)
  request["Cookie"] = auth()
  contact = {}
  contact["to_entity_id"] = contact_id.to_i
  contact["to_entity_type"] = "contacts"
  data = []
  data[0] = contact

  txt = data.to_json
  request.body = txt
  puts "до отправки, сформированный запрос, связь с контактом"

  response = $https.request(request)
  ap JSON.parse(request.body) if request.body
  puts request.body
  puts "ответ сервера, добавление связи с контактом"
  ap JSON.parse(response.read_body) if response.read_body

end
private
  def open_ssl_con(uri)
    $https = Net::HTTP.new(uri.host, uri.port);
    $https.use_ssl = true
  end
